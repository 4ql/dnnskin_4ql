$(function () {
    $('header').addClass('headerBig')

    $(window).scroll(function () {


        if ($(document).scrollTop() > 0) {
            if ($('header').hasClass('headerBig')) {
                $('header').removeClass('headerBig').addClass('headerSmall');
            }
        } else {
            if ($('header').hasClass('headerSmall')) {
                $('header').removeClass('headerSmall').addClass('headerBig');
            }
        }
    })
});