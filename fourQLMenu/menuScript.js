$(function () {
    //Activate Whole Menu
    $('nav.FourQL_menu .FourQL_menuActivate').click(function () {
        if (!$(this).next('ul').hasClass('shown')) {
            $(this).text('x').css('background', 'red');
            $(this).next('ul').addClass('shown');
        } else {
            $(this).text('menu').css('background', '#4f6384');
            $(this).next('ul').removeClass('shown');
        }
    });

    //Activate submenus
    $('nav.FourQL_menu ul').find('li a.submenu').click(function () {
        var submenu = $(this).next('ul');
        if (submenu.hasClass('shown')) {
            $(this).removeClass('open');
            submenu.slideUp('fast').removeClass('shown');
        } else {
            $(this).addClass('open');
            submenu.slideDown('fast').addClass('shown');
        }
    });

});