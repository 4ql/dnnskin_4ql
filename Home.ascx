<%@ Control language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="DNNLINK" Src="~/Admin/Skins/DnnLink.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<dnn:META ID="mobileScale" runat="server" Name="viewport" Content="width=device-width,initial-scale=1" />
<dnn:DnnJsInclude ID="HeaderShrink" runat="server" FilePath="js/HeaderShrink.js" PathNameAlias="SkinPath" AddTag="false" />
<div class="_4ql _4ql-home">
	<header>
		<div class="FourQL_headerwrap">
			<h1>4ql<small>ltd</small></h1>
			<p>more than just a website..</p>
			<dnn:MENU ID="fourQLMenu" MenuStyle="FourQLmenu" runat="server"></dnn:MENU>
		</div>
	</header>
	<div class="FourQL_contentwrap">
		<section class="FourQL_FullWidthHome">
			<div class="MainSlogan">Ingenious websites with clever features</div>
		</section>
		<section class="FourQL_FullWidth">
			<div id="ContentPane" runat="server" class="FourQL_ColumnInner"></div>
		</section>
		<section class="FourQL_TwoColumnLeft Main">
			<div id="ContentPane2" runat="server" class="FourQL_ColumnInner"></div>
		</section>
		<section class="FourQL_TwoColumnRight sidebar">
			<div id="ContentPane3" runat="server" class="FourQL_ColumnInner"></div>
		</section>
		<section class="FourQL_TwoColumnRight  Main">
			<div id="ContentPane4" runat="server" class="FourQL_ColumnInner"></div>
		</section>
		<section class="FourQL_TwoColumnLeft sidebar">
			<div id="ContentPane5" runat="server" class="FourQL_ColumnInner"></div>
		</section>
		<section class="FourQL_FullWidth">
			<div id="ContentPane6" runat="server" class="FourQL_ColumnInner"></div>
		</section>
	</div>
	<footer>
		<div class="FourQL_footerwrap">
			<div class="FourQL_footerInner">
				<div id="login" class="fourQL_Login">
					<dnn:LOGIN ID="dnnLogin" CssClass="LoginLink" runat="server" LegacyMode="false" />
				</div>
				<div class="fourql_terms">
					<a href="/dnn_platform/portals/_default/skins/4ql/#" class="fourql_designer"><small>designed by: </small>4QL <small>Ltd</small></a><br/>
					<a href="/dnn_platform/portals/_default/skins/4ql/#"><small>Terms and Conditions</small></a></div>
			</div>
		</div>
	</footer>
</div>